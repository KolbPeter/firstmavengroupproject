Needed modules:

Config module
=============

Programmers (max 2 persons):

task: need to read app configuration from a textfile with https://commons.apache.org/proper/commons-configuration/ 
expectations: 
    - working & tested module 
    - presentation (max 10minutes) about working 
    - demo 
    - Javadoc


JPA module
==========

Programmers (max 2 persons):

task: need to store data (domains) to a database using JPA https://docs.jboss.org/hibernate/orm/5.2/userguide/html_single/Hibernate_User_Guide.html

expectations:
    - working & tested module
    - presentation (max 10minutes) about working 
    - demo 
    - Javadoc
 

WEB interface
=============

Programmers (max 2 persons):

task: web interface with JSF https://www.tutorialspoint.com/jsf/index.htm

expectations:
    - working & tested module
    - presentation (max 10minutes) about working 
    - demo 
    - Javadoc

