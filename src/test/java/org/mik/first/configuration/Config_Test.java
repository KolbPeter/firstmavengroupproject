package org.mik.first.configuration;

import org.junit.Assert;
import org.junit.Test;

public class Config_Test{

    class ConfigClient implements ValueProcessor{
        @Value(name = "name", defaultValue = "asd")
        private String name;

        @Value(name = "maxConnections",defaultValue = "50")
        private int maxConnections;

        public ConfigClient() throws Exception {
            Config.initConfig("app.properties");
            processAnnotations();
        }
    }

    @Test
    public void configTest(){
        try {
            ConfigClient configClient = new ConfigClient();
            Assert.assertEquals(configClient.name,"bsd");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}
