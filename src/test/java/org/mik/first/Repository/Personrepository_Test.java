package org.mik.first.Repository;

import org.junit.Assert;
import org.junit.Test;
import org.mik.first.domain.Country;
import org.mik.first.domain.Person;

public class Personrepository_Test {

    private static final String COUNTRY_NAME = "Hungary";
    private static final String COUNTRY_SIGN = "HU";

    private static final String SLARTIBARTFAST = "Slartibartfast";
    private static final String S_ID = "42";
    private static final String S_ADDRESS = "Magrathea";

    private static final String ARTHUR = "Arthur Dent";
    private static final String AADDRESS = "Earth";
    private static final String A_ID = "43";

    @Test
    public  void crudTest() throws Exception {
        CountryRepository countryRepository = CountryRepository.getInstance();
        Country country = new Country(COUNTRY_NAME,COUNTRY_SIGN);
        countryRepository.save(country);

        PersonRepository personRepository = PersonRepository.getInstance();
        Assert.assertEquals(personRepository.getCount(),0);

        Person person = new Person(null,SLARTIBARTFAST,S_ADDRESS,S_ID,country);
        Assert.assertNull(person.getId());

        personRepository.save(person);
        Assert.assertEquals(personRepository.getCount(),1);

        Assert.assertNotNull(person.getId());
        Assert.assertEquals(person.getName(),SLARTIBARTFAST);
        Assert.assertEquals(person.getAddress(),S_ADDRESS);
        Assert.assertEquals(person.getIdNumber(),S_ID);

        Person byID = personRepository.getByID(person.getId());
        compare(person,byID);
        person.setName(ARTHUR);
        person.setAddress(AADDRESS);
        person.setIdNumber(A_ID);

        personRepository.save(person);

        byID = personRepository.getByID(person.getId());
        compare(person,byID);

        personRepository.delete(person.getId());

        Assert.assertEquals(personRepository.getCount(),0);

    }

    private void compare(Person p1, Person p2){
        Assert.assertEquals(p1.getName(),p2.getName());
        Assert.assertEquals(p1.getAddress(),p2.getAddress());
        Assert.assertEquals(p1.getIdNumber(),p2.getIdNumber());
        Assert.assertEquals(p1.getId().compareTo(p2.getId()),0);
    }
    
}
