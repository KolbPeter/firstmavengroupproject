package org.mik.first.Repository;

import org.junit.Assert;
import org.junit.Test;
import org.mik.first.domain.Country;
import org.mik.first.domain.Company;

public class Companyrepository_Test {
    private static final String COUNTRY_NAME = "Hungary";
    private static final String COUNTRY_SIGN = "HU";

    private static final String SCC = "Sirius Cybernetics Corporation";
    private static final String S_TAX = "42";
    private static final String S_ADDRESS = "Sirius Tau";

    private static final String VCF = "Vogon Constructor Fleet";
    private static final String V_TAX = "43";
    private static final String V_ADDRESS = "Vogsphere";

    @Test
    public  void crudTest() throws Exception {
        CountryRepository countryRepository = CountryRepository.getInstance();
        Country country = new Country(COUNTRY_NAME,COUNTRY_SIGN);
        countryRepository.save(country);

        CompanyRepository companyRepository = CompanyRepository.getInstance();
        Assert.assertEquals(companyRepository.getCount(),0);

        Company company = new Company(null,SCC,S_ADDRESS,S_TAX,country);
        Assert.assertNull(company.getId());

        companyRepository.save(company);
        Assert.assertEquals(companyRepository.getCount(),1);

        Assert.assertNotNull(company.getId());
        Assert.assertEquals(company.getName(),SCC);
        Assert.assertEquals(company.getAddress(),S_ADDRESS);
        Assert.assertEquals(company.getTaxNumber(),S_TAX);

        Company byID = companyRepository.getByID(company.getId());
        compare(company,byID);
        company.setName(VCF);
        company.setAddress(V_ADDRESS);
        company.setTaxNumber(V_TAX);

        companyRepository.save(company);

        byID = companyRepository.getByID(company.getId());
        compare(company,byID);

        companyRepository.delete(company.getId());

        Assert.assertEquals(companyRepository.getCount(),0);

    }

    private void compare(Company p1, Company p2){
        Assert.assertEquals(p1.getName(),p2.getName());
        Assert.assertEquals(p1.getAddress(),p2.getAddress());
        Assert.assertEquals(p1.getTaxNumber(),p2.getTaxNumber());
        Assert.assertEquals(p1.getId().compareTo(p2.getId()),0);
    }

}
