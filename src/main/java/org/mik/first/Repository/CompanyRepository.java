package org.mik.first.Repository;

import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.mik.first.domain.Company;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class CompanyRepository extends AbstractRepository< Long, Company> {
    private static CompanyRepository instance;

    private CompanyRepository(){}

    public static synchronized CompanyRepository getInstance(){
        return instance == null ? instance=new CompanyRepository():instance;
    }

    @Override
    protected Class<Company> getClazz() {
        return Company.class;
    }

    public Company getByTaxNumber(String taxNumber) throws Exception {
        if ( taxNumber.isEmpty()) return null;
        try (Connection connection = new Connection()) {
            Transaction transaction = connection.session.beginTransaction();
            CriteriaBuilder criteriaBuilder = connection.session.getCriteriaBuilder();
            CriteriaQuery<Company> criteriaQuery = criteriaBuilder.createQuery(Company.class);
            Root<Company> root = criteriaQuery.from(Company.class);
            criteriaQuery.select(root).where(criteriaBuilder.equal(root.get(Company.FLD_TAX_NUMBER),taxNumber));
            Query<Company> query = connection.session.createQuery(criteriaQuery);
            return query.getSingleResult();
        }
    }
}
