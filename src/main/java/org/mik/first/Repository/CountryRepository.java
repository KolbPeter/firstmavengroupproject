package org.mik.first.Repository;

import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.mik.first.domain.Country;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class CountryRepository extends AbstractRepository<Long, Country> {

    private static CountryRepository instance;

    private CountryRepository(){}

    public static synchronized CountryRepository getInstance(){
        return instance == null ? instance=new CountryRepository():instance;
    }

    public Country getCountryByName(String name) throws Exception {
        if ( name.isEmpty()) return null;
        try (Connection connection = new Connection()) {
            Transaction transaction = connection.session.beginTransaction();
            CriteriaBuilder criteriaBuilder = connection.session.getCriteriaBuilder();
            CriteriaQuery<Country> criteriaQuery = criteriaBuilder.createQuery(Country.class);
            Root<Country> root = criteriaQuery.from(Country.class);
            criteriaQuery.select(root).where(criteriaBuilder.equal(root.get(Country.FLD_NAME),name));
            Query<Country> query = connection.session.createQuery(criteriaQuery);
            return query.getSingleResult();
        }
    }


    @Override
    protected Class<Country> getClazz() {
        return Country.class;
    }
}
