package org.mik.first.Repository;

import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.mik.first.domain.Person;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class PersonRepository extends AbstractRepository<Long, Person> {
    private static PersonRepository instance;

    private PersonRepository(){}

    public static synchronized PersonRepository getInstance(){
        return instance == null ? instance=new PersonRepository():instance;
    }

    @Override
    protected Class<Person> getClazz() {
        return Person.class;
    }

    public Person getByPersonalID(String id) throws Exception {
        if ( id.isEmpty()) return null;
        try (Connection connection = new Connection()) {
            Transaction transaction = connection.session.beginTransaction();
            CriteriaBuilder criteriaBuilder = connection.session.getCriteriaBuilder();
            CriteriaQuery<Person> criteriaQuery = criteriaBuilder.createQuery(Person.class);
            Root<Person> root = criteriaQuery.from(Person.class);
            criteriaQuery.select(root).where(criteriaBuilder.equal(root.get(Person.FLD_PERSONAL_ID),id));
            Query<Person> query = connection.session.createQuery(criteriaQuery);
            return query.getSingleResult();
        }
    }

}
