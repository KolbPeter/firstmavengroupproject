package org.mik.first.Repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.mik.first.Hibernate.HibernateUtil;
import org.mik.first.domain.AbstractEntity;
import org.mik.first.domain.Client;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Closeable;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public abstract class AbstractRepository<ID, T extends AbstractEntity<ID>> {

    static class Connection implements Closeable{
        public Session session;

        public Connection(){
            session = HibernateUtil.getSessionFactory().getCurrentSession();
        }

        @Override
        public void close() throws IOException {
            session.close();
        }
    }

    protected abstract Class<T> getClazz();

    public void save(T entity) throws  Exception{
        if (entity == null) return;

        try (Connection connection = new Connection()) {
            Transaction transaction = connection.session.beginTransaction();
            try {
                connection.session.save(entity);
            }catch (Exception ex){
                transaction.rollback();
                throw ex;
            }finally {
                if (transaction.getStatus()== TransactionStatus.ACTIVE){
                    transaction.commit();
                }
            }

        }
    }

    public T getByID(ID id) throws Exception {
        if (id==null) return null;
        try (Connection connection = new Connection()) {
            Transaction transaction = connection.session.beginTransaction();
            return  connection.session.find(getClazz(),id);
        }
    }

    public List<T> getByName(String name) throws Exception {
        if ( name.isEmpty()) return Collections.emptyList();
        try (Connection connection = new Connection()) {
            Transaction transaction = connection.session.beginTransaction();
            Class<T> clazz = getClazz();
            CriteriaBuilder criteriaBuilder = connection.session.getCriteriaBuilder();
            CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
            Root<T> root = criteriaQuery.from(clazz);
            criteriaQuery.select(root).where(criteriaBuilder.equal(root.get(Client.FLD_ADDRESS),name));
            Query<T> query = connection.session.createQuery(criteriaQuery);
            return query.getResultList();
        }
    }

    public long getCount() throws Exception {
        try (Connection connection = new Connection()) {
            Transaction transaction = connection.session.beginTransaction();
            CriteriaBuilder criteriaBuilder = connection.session.getCriteriaBuilder();
            CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
            Root<T> root = criteriaQuery.from(getClazz());
            criteriaQuery.select(criteriaBuilder.count(root.get(Client.FLD_ID)));
            return connection.session.createQuery(criteriaQuery).getSingleResult();
        }
    }

    public void delete(ID id) throws Exception {
        if (id == null) return;
        try (Connection connection = new Connection()) {
            Transaction transaction = connection.session.beginTransaction();
            try {
                T obj = connection.session.find(getClazz(),id);
                if (obj == null) return;
                connection.session.delete(obj);
            }catch (Exception ex){
                transaction.rollback();
                throw ex;
            }finally {
                if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) transaction.commit();
            }
        }
    }
}
