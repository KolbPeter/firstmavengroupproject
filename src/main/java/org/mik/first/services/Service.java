package org.mik.first.services;

import org.mik.first.domain.Client;

public interface Service<T extends Client> {

    void pay(T  client);

    void receiveService(T client);

}
