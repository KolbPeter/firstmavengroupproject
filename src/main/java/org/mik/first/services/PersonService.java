package org.mik.first.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Person;
import org.mik.first.export.json.JSONGenerator;
import org.mik.first.export.xml.XMLGenerator;

public class PersonService implements Service<Person> {

    private final static Logger LOG  = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private XMLGenerator xmlGenerator = new XMLGenerator();
    private JSONGenerator jsonGenerator = new JSONGenerator();

    @Override
    public void pay(Person client) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter PersonService. client:"+client);
        System.out.println("Enter PersonService. client:"+client);
        System.out.println("Enter PersonService.pay " + client + xmlGenerator.convert2XML(client));
        System.out.println("Enter PersonService.pay " + client + jsonGenerator.convert2JSON(client));
    }

    @Override
    public void receiveService(Person client) {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter PersonService.receiveService client:" + client);
        System.out.println("Enter PersonService.receiveService client:" + client);
    }
}
