package org.mik.first;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.Repository.CountryRepository;
import org.mik.first.domain.Client;
import org.mik.first.domain.Company;
import org.mik.first.domain.Person;
import org.mik.first.services.CompanyService;
import org.mik.first.services.PersonService;


import java.util.ArrayList;
import java.util.List;

public class Control {

    private final static Logger LOG  = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private final PersonService personService;
    private final CompanyService companyService;

    private long id;

    public Control() {
        this.personService = new PersonService();
        this.companyService = new CompanyService();
        this.id=0;
    }

    public void start() throws Exception {
        List<String[]> dummyList = createDummyList();
        Client client;
        for (String[] strings: dummyList) {
            client=this.convertFromString(strings);
            this.dewIt(client);
        }
    }

    private Client convertFromString(String[] strings) throws Exception {
        CountryRepository cr = CountryRepository.getInstance();
        switch (strings[0]) {
           case  "P" : return new Person(id++, strings[1], strings[2], strings[3], cr.getCountryByName(strings[4]));
           case  "C" : return new Company(id++, strings[1], strings[2], strings[3], cr.getCountryByName(strings[4]));
            default: if (DEBUG_TEMPORARY)
               LOG.debug("Control.convertFromString error: unknown flag "+strings[0]);
               throw  new RuntimeException("Control.convertFromString error: unknown flag"+strings[0]);
        }
    }

    private void dewIt(Client client) {
        if (client instanceof Person) {
            this.personService.pay((Person) client);
            return;
        }
        if (client instanceof Company) {
            this.companyService.pay((Company) client);
            return;
        }
        throw  new RuntimeException("Unknown client");
    }

    private List<String[]> createDummyList() {
        List<String[]> dummyList = new ArrayList<>();
        dummyList.add(new String[] { "P", "Zaphod Beeblebrox", "Betelgeuse", "42234560TA", "Betelgeuse"});
        dummyList.add(new String[] { "P", "Linus Torvalds", "USA", "42234560TL", "USA"});
        dummyList.add(new String[] { "P", "Tricia McMillan", "Earth", "42234560TM", "Earth"});
        dummyList.add(new String[] { "P", "Ford Prefect", "Betelgeuse", "42234560TB", "Betelgeuse"});
        dummyList.add(new String[] { "C", "Google", "USA", "43234560TL", "USA"});
        dummyList.add(new String[] { "C", "Oracle", "USA", "43234560TO", "USA"});
        dummyList.add(new String[] { "C", "Microsoft", "USA", "4344566560TL", "USA"});
        dummyList.add(new String[] { "C", "KGB", "Russia", "43134560TL", "Russia"});

        return dummyList;
    }
}





















