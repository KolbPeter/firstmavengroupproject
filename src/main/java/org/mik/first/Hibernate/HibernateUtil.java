package org.mik.first.Hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.mik.first.domain.Client;
import org.mik.first.domain.Company;
import org.mik.first.domain.Country;
import org.mik.first.domain.Person;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory(){
        if (sessionFactory==null){
            try {
                Configuration configuration = new Configuration();
                Properties settings = new Properties();
                settings.put(Environment.DRIVER, "org.hsqldb.jdbc.JDBCDriver");
                settings.put(Environment.URL,"jdbc:hsqldb:mem:db/mp;hsqldb.log_data=false");
                settings.put(Environment.USER,"sa");
                settings.put(Environment.PASS,"");
                settings.put(Environment.DIALECT,"");
                settings.put(Environment.SHOW_SQL,"true");
                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS,"thread");
                settings.put(Environment.ALLOW_UPDATE_OUTSIDE_TRANSACTION,"true");
                settings.put(Environment.HBM2DDL_AUTO,"create-drop");
                configuration.setProperties(settings);
                configuration.addAnnotatedClass(Person.class);
                configuration.addAnnotatedClass(Client.class);
                configuration.addAnnotatedClass(Country.class);
                configuration.addAnnotatedClass(Company.class);
                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            }catch (Exception ex){
                ex.printStackTrace();
                System.exit(-1);
            }
        }
        return sessionFactory;
    }
}
