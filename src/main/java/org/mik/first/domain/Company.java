package org.mik.first.domain;

import org.mik.first.export.json.JSONElement;
import org.mik.first.export.json.JSONSerializable;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerializable;

import javax.persistence.Column;
import javax.persistence.Entity;

@XMLSerializable
@JSONSerializable
@Entity(name = Company.TBL_NAME)
public class Company extends Client {

    public static final String TBL_NAME = "company";
    public static final String FLD_TAX_NUMBER = "tax_number";

    @XMLElement(key = "TaxNumber")
    @JSONElement(key = "TaxNumber")
    @Column(name = FLD_TAX_NUMBER,unique = true,nullable = false)
    private String taxNumber;

    public Company() {
        super();
    }

    public Company(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public Company(Long id, String name, String address, String taxNumber, Country country) {
        super(id, name, address,country);
        this.taxNumber = taxNumber;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;
        if (!super.equals(o)) return false;

        Company company = (Company) o;

        return taxNumber.equals(company.taxNumber);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + taxNumber.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Company{" +
                "taxNumber='" + taxNumber + '\'' +
                super.toString() +
                '}';
    }
}