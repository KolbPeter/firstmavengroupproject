package org.mik.first.domain;

import org.apache.commons.lang3.StringUtils;
import org.mik.first.export.json.JSONElement;
import org.mik.first.export.json.JSONSerializable;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@XMLSerializable(key="MyClient")
@JSONSerializable(key="MyClient")
@Entity(name = Client.TBL_NAME)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Client extends AbstractEntity<Long>{

    public static final String TBL_NAME = "client";
    public static final String FLD_NAME = "name";
    public static final String FLD_ADDRESS = "address";
    public static final String FLD_COUNTRY = "country";

    @XMLElement
    @JSONElement
    @Column(name = FLD_NAME, nullable = false, unique = true)
    @NotNull(message = "Name cannot be null")
    @Size(min = 3,max = 50,message = "name size must be between 3 and 50 chars.")
    private String name;

    @XMLElement
    @JSONElement
    @Column(name = FLD_ADDRESS, nullable =  false)
    @NotNull(message = "Address cannot be null!")
    @Size(min = 2,max = 50,message = "Address size must be between 2 and 50 chars.")
    private String address;

    @XMLElement
    @JSONElement
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = FLD_COUNTRY)
    private Country country;

    public Client() {

    }

    public Client(Long id, String name, String address, Country country) {
        super(id);
        this.name = name;
        this.address = address;
        this.country = country;
    }

    public Long getId() {
        return super.getId();
    }

    public void setId(Long id) {
        super.setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name==null || name.isEmpty() || StringUtils.isBlank(name))
            return;

        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(name, client.name) &&
                Objects.equals(address, client.address) &&
                Objects.equals(country, client.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, country);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + super.getId() +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
