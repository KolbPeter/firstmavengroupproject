package org.mik.first.domain;

import org.mik.first.export.json.JSONElement;
import org.mik.first.export.json.JSONSerializable;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Objects;

@XMLSerializable
@JSONSerializable
@Entity(name = Person.TBL_NAME)
public class Person extends Client {

    public static final String TBL_NAME = "person";
    public static final String FLD_PERSONAL_ID = "personal_id";

    @XMLElement(key = "PersonalId")
    @JSONElement(key = "PersonalId")
    @Column(name = FLD_PERSONAL_ID,unique = true,nullable = false)
    private String idNumber;

    public Person() {
        super();
    }

    public Person(String idNumber) {
        this.idNumber = idNumber;
    }

    public Person(Long id, String name, String address, String idNumber, Country country) {
        super(id, name, address,country);
        this.idNumber = idNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Person person = (Person) o;
        return Objects.equals(idNumber, person.idNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), idNumber);
    }

    @Override
    public String toString() {
        return "Person{" +
                "idNumber='" + idNumber + '\'' +
                super.toString() +
                '}' ;
    }
}