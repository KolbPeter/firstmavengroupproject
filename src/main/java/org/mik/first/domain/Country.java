package org.mik.first.domain;

import org.mik.first.export.json.JSONElement;
import org.mik.first.export.json.JSONSerializable;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@XMLSerializable
@JSONSerializable
@Entity(name = Country.TBL_NAME)
public class Country extends AbstractEntity<Long> {

    public static final String TBL_NAME = "country";
    public static final String FLD_NAME = "name";
    public static final String FLD_SIGN = "sign";

    @XMLElement
    @JSONElement
    @Column(name = FLD_NAME,nullable = false)
    @NotNull(message = "Name cannot be null!")
    @Size(min = 3,max = 50,message = "Length must be between 3 and 50 chars!")
    private String name;

    @XMLElement
    @JSONElement
    @Column(name = FLD_SIGN,nullable = false)
    @NotNull(message = "Sign cannot be null!")
    @Size(min = 1,max = 5,message = "Length must be between 1 and 5 chars!")
    private String sign;

    @OneToMany(mappedBy = Client.FLD_COUNTRY,orphanRemoval = true)
    private List<Client> clients;

    public Country() {
    }

    public Country(String name, String sign) {
        super();
        this.name = name;
        this.sign = sign;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Objects.equals(name, country.name) &&
                Objects.equals(sign, country.sign);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sign);
    }
}

