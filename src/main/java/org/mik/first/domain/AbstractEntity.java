package org.mik.first.domain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.mik.first.export.json.JSONElement;
import org.mik.first.export.xml.XMLElement;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class AbstractEntity<ID> {

    public static final String FLD_ID="id";

    @Id
    @XMLElement(key = "Identifier")
    @JSONElement(key = "Identifier")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = FLD_ID)
    private ID id;

    @CreationTimestamp
    private LocalDateTime created;

    @UpdateTimestamp
    private LocalDateTime updated;

    @Version
    private int version;

    public AbstractEntity() {
    }

    public AbstractEntity(ID idNumb) {
        this.id = idNumb;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }
}
