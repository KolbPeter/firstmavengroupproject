package org.mik.first;

import org.mik.first.domain.Client;
import org.mik.first.domain.Person;

public class Main {

    public static void main(String[] main) {
        try {
            new Control().start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
