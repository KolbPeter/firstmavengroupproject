package org.mik.first.export.json;

public class JSONSerializationException extends RuntimeException {

    public JSONSerializationException() {
    }

    public JSONSerializationException(String message) {
        super(message);
    }

    public JSONSerializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public JSONSerializationException(Throwable cause) {
        super(cause);
    }

    public JSONSerializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
