package org.mik.first.export.json;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class JSONGenerator {
    private static final String NEWLINE = System.getProperty("line.separator");

    public String convert2JSON(Object object) throws JSONSerializationException {
        try {
            StringBuilder sb = new StringBuilder(NEWLINE);
            process(object, sb);
            return sb.toString();
        }
        catch (Exception e) {
            throw new JSONSerializationException(e.getMessage(), e);
        }
    }

    private void process(Object object, StringBuilder sb) throws
                IllegalArgumentException, IllegalAccessException,
                InvocationTargetException {

        checkIfSerializable(object);
        initJSON(object);
        convert2String(object, sb);
    }

    private void checkIfSerializable(Object object) {
        if (object==null)
            throw new JSONSerializationException("Object is null");
        Class<?> clazz = object.getClass();
        if (!clazz.isAnnotationPresent(JSONSerializable.class))
            throw new JSONSerializationException(clazz.getName() +
                    " object cannot serializable to JSON");
    }

    private void initJSON(Object object) throws IllegalAccessException, InvocationTargetException {
        if (object==null)
            throw new JSONSerializationException("Object is null");
        Class<?> clazz = object.getClass();
        List<Method> methods =new ArrayList<>();
        Collections.addAll(methods, clazz.getDeclaredMethods());
        for (Method m:methods) {
            if (m.isAnnotationPresent(JSONInit.class)) {
                m.setAccessible(true);
                m.invoke(object);
            }
        }
    }

    private void convert2String(Object object, StringBuilder sb)
            throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        if (object==null)
            throw new JSONSerializationException("Object is null");
        Class<?> clazz = object.getClass();
        JSONSerializable JSONSerializable = clazz.getAnnotation(JSONSerializable.class);
        String classKey = "".equals(JSONSerializable.key())
                ? clazz.getSimpleName().toUpperCase()
                : JSONSerializable.key();
        sb.append('"').append(classKey).append('"').append(":{").append(NEWLINE);
        List<Field> fields = new ArrayList<>();
        Collections.addAll(fields, clazz.getDeclaredFields());
        addParentFields(clazz, fields);
        for(Field f:fields) {
            if (f.isAnnotationPresent(JSONElement.class)) {
                f.setAccessible(true);
                JSONElement element = f.getAnnotation(JSONElement.class);
                String key = "".equals(element.key())
                        ? f.getName().toUpperCase()
                        : element.key();
                Object value = f.get(object);
                sb.append('"').append(key).append('"').append(':');
                if (value!=null && value.getClass()
                        .isAnnotationPresent(JSONSerializable.class)) {
                    sb.append("[").append(NEWLINE);
                    process(value, sb);
                    sb.append(NEWLINE).append("],").append(NEWLINE);
                }
                else {
                    sb.append('"').append(Objects.toString(value)).append('"');
                }
                sb.append(",").append(NEWLINE);
            }
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append("}").append(NEWLINE);
    }

    private void addParentFields(Class<?> clazz, List<Field> fields) {
        Class<?> superClass = clazz.getSuperclass();
        if (superClass.isAnnotationPresent(JSONSerializable.class)) {
            Collections.addAll(fields, superClass.getDeclaredFields());
            addParentFields(superClass, fields);
        }
    }
}
