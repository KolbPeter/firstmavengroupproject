package org.mik.first.export.json;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface JSONSerializable {
    public String key() default "";
}
