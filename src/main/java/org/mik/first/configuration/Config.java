package org.mik.first.configuration;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;

import java.io.File;

public class Config {
    private Configurations configurations = new Configurations();
    private static Configuration configuration;

    private Config(String filename) throws Exception {
        configuration = configurations.properties(new File(filename));
    }

    public static synchronized void initConfig(String fname) throws Exception {
        new Config(fname);
    }

    public static synchronized Configuration getConfig(){
        return configuration;
    }
}
